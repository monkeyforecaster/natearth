package natearth

import (
	geo "bitbucket.org/monkeyforecaster/geometry"
	"fmt"	
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

var tableMap map[string]string = map[string]string{"provinces": "ne_50m_admin_1_states_provinces_shp",
                               "countries":"ne_50m_admin_0_countries"}

func NNamesTable(name, tableName string, n int) ([]string, error) {

	db, err := sql.Open("sqlite3", "/mnt/natearth/natural_earth_vector.sqlite")
	if err != nil {
		return nil, err
	}
	defer db.Close()

	rows, err := db.Query(fmt.Sprintf("SELECT name FROM %s WHERE name LIKE '%%%s%%' LIMIT %d;", tableName, name, n))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	out := []string{}
	var entry string
	for rows.Next() {
		err = rows.Scan(&entry)
		if err != nil {
			return nil, err
		}
		out = append(out, entry)
	}
	return out, nil
}

func NNames(name string, n int) ([]string, error) {

	countryRes, err := NNamesTable(name, tableMap["countries"], n)
	if err != nil {
		return nil, err
	}
	regionRes, err := NNamesTable(name, tableMap["provinces"], n)
	if err != nil {
		return nil, err
	}
	
	out := []string{}
	out = append(out, countryRes...)
	out = append(out, regionRes...)
	
	if len(out) < n {
		return out, nil
	} else {
		return out[:n], nil
	}
}

func GetGeometryTable(name, tableName string) (geo.Geometry, error) {

	db, err := sql.Open("sqlite3", "/mnt/natearth/natural_earth_vector.sqlite")
	if err != nil {
		return nil, err
	}
	defer db.Close()
	
	rows, err := db.Query(fmt.Sprintf("SELECT GEOMETRY FROM %s WHERE name='%s' LIMIT 1;", tableName, name))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var wkbMsg sql.RawBytes

	rows.Next()
	err = rows.Scan(&wkbMsg)
	if err != nil {
		return nil, err
	}
	
	var poly geo.Polygon
	err = poly.UnmarshalWKB(wkbMsg)
	if err == nil {
		return poly, nil
	}

	var mpoly geo.MultiPolygon
	err = mpoly.UnmarshalWKB(wkbMsg)
	if err != nil {
		return nil, err
	}
	return mpoly, nil
}


func GetGeometry(name string) (geo.Geometry, error) {

	geom, err := GetGeometryTable(name, tableMap["countries"])
	if err == nil {
		return geom, err
	}
	geom, err = GetGeometryTable(name, tableMap["provinces"])
	if err != nil {
		return nil, err
	}
	return geom, err
}
